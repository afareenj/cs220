// test_search_functions.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
// Afareen Jaleel
// ajaleel3


#include <stdio.h>
#include <assert.h>
#include "search_functions.h"


/* 
 * Declarations for tester functions whose definitions appear below.
 * (You will need to fill in the function definition details, at the
 * end of this file, and add comments to each one.) 
 * Additionally, for each helper function you elect to add to the 
 * provided search_functions.h, you will need to supply a corresponding
 * tester function in this file.  Add a declaration for it here, its
 * definition below, and a call to it where indicated in main.
 */
void test_file_eq();      // This one is already fully defined below.
void test_populate_grid();
void test_find_right();
void test_find_left();
void test_find_down();
void test_find_up();
void test_find_all();


/*
 * Main method which calls all test functions.
 */
int main() {
  printf("Testing file_eq...\n");
  test_file_eq();
  printf("Passed file_eq test.\n\n");

  printf("Running search_functions tests...\n");
  printf("Testing populate_grid\n");
  printf("The error messages printed out by function should be: \nGrid file failed to open.Invalid grid.Invalid grid.Invalid grid.Invalid grid\n");
  printf("The error messages from running function:\n");
  test_populate_grid();
  test_find_right();
  test_find_left();
  test_find_down();
  test_find_up();
  test_find_all();

  /* You may add calls to additional test functions here. */
  
  printf("\nPassed search_functions tests!!!\n");
  printf("All search_functions tests passed!\n");
}



/*
 * Test file_eq on same file, files with same contents, files with
 * different contents and a file that doesn't exist.
 * Relies on files test1.txt, test2.txt, test3.txt being present.
 */
void test_file_eq() {
  FILE* fptr = fopen("test1.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  fptr = fopen("test2.txt", "w");
  fprintf(fptr, "this\nis\na different test\n");
  fclose(fptr);

  fptr = fopen("test3.txt", "w");
  fprintf(fptr, "this\nis\na test\n");
  fclose(fptr);

  assert( file_eq("test1.txt", "test1.txt"));
  assert( file_eq("test2.txt", "test2.txt"));
  assert(!file_eq("test2.txt", "test1.txt"));
  assert(!file_eq("test1.txt", "test2.txt"));
  assert( file_eq("test3.txt", "test3.txt"));
  assert( file_eq("test1.txt", "test3.txt"));
  assert( file_eq("test3.txt", "test1.txt"));
  assert(!file_eq("test2.txt", "test3.txt"));
  assert(!file_eq("test3.txt", "test2.txt"));
  assert(!file_eq("", ""));  // can't open file
}



void test_populate_grid(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  assert(populate_grid(testGrid,"")==-1); //checks if program returns error when file empty or unable to open file
  assert(populate_grid(testGrid,"test2.txt")==-2); //checks if program returns correct error value when file has lines with more than 10 chars
  assert(populate_grid(testGrid,"test4.txt")==-2); //checks if program returns correct error value when file has rows and columns of length 0
  assert(populate_grid(testGrid,"test1.txt")==-2); //checks if program returns correct error value when file has lines of different lengths
  assert(populate_grid(testGrid,"test5.txt")==-2); //checks if program returns correct error value when file has different number of rows than columns
  int n = populate_grid(testGrid,"grid2.txt"); 
  assert(n==10);//checks if function works for 10x10 grid and returns correct dimensions
  n = populate_grid(testGrid,"grid3.txt");
  assert(n==1);//checks if functions works for 1x1 grid and returns correct dimension
  n = populate_grid(testGrid,"grid.txt");
  assert(n==4); //check that the program is returning the correct value for array dimension (for grid.txt, n=4)
  FILE *output;
  output = fopen("output.txt","w");
  for(int a = 0; a<n; a++)
    fprintf(output,"%s\n",testGrid[a]);//prints the strings of the testGrid array into output.txt file
  fclose(output);
  assert( file_eq("output.txt", "test0.txt"));//check that the elements of char array testGrid match those of what the char array should be (as shown by test0.txt)
}


void test_find_right(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  int num = populate_grid(testGrid,"grid.txt");
  FILE *output;
  output = fopen("output.txt","w");
  assert(!find_right(testGrid,num,"pop",output));//checks if program returns 0 when word pop is not found in sample file grid.txt
  find_right(testGrid,num,"key",output);
  fclose(output);
  assert( file_eq("test6.txt","output.txt"));//checks if program finds right facing word in sample file grid.txt and returns correct output into output.txt
  
}


void test_find_left(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  int num = populate_grid(testGrid,"grid.txt");
  FILE *output;
  output = fopen("output.txt","w");
  assert(!find_left(testGrid,num,"pop",output));//checks if program returns 0 when word pop is not found in sample file grid.txt
  find_left(testGrid,num,"tip",output);
  fclose(output);
  assert( file_eq("test7.txt","output.txt"));//checks if program finds left facing word in sample file grid.txt and returns correct output into output.txt
}


void test_find_down(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  int num = populate_grid(testGrid,"grid.txt");
  FILE *output;
  output = fopen("output.txt","w");
  assert(!find_down(testGrid,num,"tip",output));//checks if program returns 0 when word tip is not found in sample file grid.txt
  find_down(testGrid,num,"pop",output);
  find_down(testGrid,num,"key",output);
  fclose(output);
  assert( file_eq("test8.txt","output.txt"));//checks if program finds down facing word in sample file grid.txt and returns correct output into output.txt
}


void test_find_up(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  int num = populate_grid(testGrid,"grid.txt");
  FILE *output;
  output = fopen("output.txt","w");
  assert(!find_down(testGrid,num,"tip",output));//checks if program returns 0 when word tip is not found in sample file grid.txt
  output = fopen("output.txt","w");
  find_up(testGrid,num,"pop",output);
  fclose(output);
  assert( file_eq("test9.txt","output.txt"));//checks if program finds up facing word in sample file grid.txt and returns correct output into output.txt
}


void test_find_all(){
  char testGrid[MAX_SIZE+2][MAX_SIZE+2];
  int num = populate_grid(testGrid,"grid.txt");
  FILE *output;
  output = fopen("output.txt","w");
  char temp[] = "tip";
  find_all(testGrid,num,temp,output);
  char temp1[] = "pop";
  find_all(testGrid,num,temp1,output);
  char temp2[] = "key";
  find_all(testGrid,num,temp2,output);
  char temp3[] = "nope";
  find_all(testGrid,num,temp3,output);
  fclose(output);
  assert( file_eq("test10.txt","output.txt"));//checks if program finds all instances of tip,pop,key, and nope in sample file grid.txt
  output = fopen("output.txt","w");
  num = populate_grid(testGrid,"grid2.txt");
  find_all(testGrid,num,temp1,output);//program finds all instances of word pop
  char temp4[] = "POPOpOPoP";
  find_all(testGrid,num,temp4,output);//programs finds word with upper and lowercase
  char temp5[] = "popopopopa";
  find_all(testGrid,num,temp5,output);//program finds word of length 10
  char temp6[] = "z";
  find_all(testGrid,num,temp6,output);//program finds word of length 1
  fclose(output);
  assert( file_eq("test11.txt","output.txt"));//checks if program finds all instances of temp1,temp4,temp5 and temp6 in sample file grid2.txt
  output = fopen("output.txt","w");
  num = populate_grid(testGrid,"grid3.txt");
  char temp0[] = "e";
  find_all(testGrid,num,temp0,output);//program finds word of length 1
  fclose(output);
  assert( file_eq("test12.txt","output.txt"));//checks if program finds all instances of a word of length 1 from a grid of dimension 1x1
}

