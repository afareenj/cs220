// search_functions.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
// Afareen Jaleel
// ajaleel3


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "search_functions.h"



/* 
 * Given a filename and a MAX_SIZExMAX_SIZE grid to fill, this function 
 * populates the grid and returns n, the actual grid dimension. 
 * If filename_to_read_from can't be opened, this function returns -1.
 * If the file contains an invalid grid, this function returns -2.
 */
int populate_grid(char grid[][MAX_SIZE+2], char filename_to_read_from[]){
  FILE *fp;
  char buffer[MAX_SIZE+2];
  fp = fopen(filename_to_read_from,"r");//open the file
  if (fp==NULL) {//if file is empty prints out error statement
    printf("Grid file failed to open.");
    return -1;
  }
  int counter = 0;
  while(fgets(buffer,MAX_SIZE+2,fp)) {
    if(strlen(buffer)>12 || strlen(buffer)==0 || buffer==NULL) {
      printf("Invalid grid.");//if file is incorrect format prints error statement
      return -2;
    }
    if(counter!=0) {
      if(strlen(buffer)!=strlen(grid[0])+1) {
	printf("Invalid grid.");//if a line is not the same length as the first line of the file 
	  return -2;
	}
    }
    for (int a = 0; a<(int)strlen(buffer)-1;a++) {
      grid[counter][a] = tolower(buffer[a]);//makes all elements in grid lowercase
    }
    grid[counter][strlen(buffer)-1] = 0;
    counter++;
  }
  fclose(fp);
  if(counter!=(int)strlen(grid[0])) {
    printf("Invalid grid.");//if file is not square dimension (i.e. nxn) prints error statement
    return -2;
  }
  return counter;//returns the dimension (i.e. n for an nxn grid)

}


/* 
 * Returns the number of times the given word was found in the grid facing the right 
 */
int find_right(char grid[][MAX_SIZE+2], int n, char word[], FILE *write_to){
  int isFound = 0;
  for (int row = 0; row<n; row++) {
    for (int col = 0; col<(int)(strlen(grid[row])-strlen(word)+1); col++) {
      if(grid[row][col]==word[0]) {
	char temp[strlen(word)];
	for (int a = 0; a<(int)strlen(word); a++)
	  temp[a] = grid[row][col+a];
	temp[strlen(word)] = 0;
	if(strcmp(temp,word)==0) {
	  fprintf(write_to,"%s %d %d R\n",word,row,col);
	  isFound++;
	}
      }
    }
  }
  return isFound;
}


/* 
 * Returns the number of times the given word was found in the grid facing the left
 */
int find_left (char grid[][MAX_SIZE+2], int n, char word[], FILE *write_to){
  int isFound = 0;
  for (int row = 0; row<n; row++) {
    for (int col = strlen(word)-1; col<(int)strlen(grid[row]); col++) {
      if(grid[row][col]==word[0]) {
	char temp[strlen(word)];
	for (int a = 0; a<(int)strlen(word); a++) {
	  temp[a] = grid[row][col-a];
	}
	temp[strlen(word)] = 0;
	if(strcmp(temp,word)==0) {
	  fprintf(write_to,"%s %d %d L\n",word,row,col);
	  isFound++;
	}
      }
    }
  }
  return isFound;

}


/* 
 * Returns the number of times the given word was found in the grid facing down 
 */
int find_down (char grid[][MAX_SIZE+2], int n, char word[], FILE *write_to){
  int isFound =0;
  for (int row = 0; row<n-(int)strlen(word)+1; row++) {
    for (int col = 0; col<(int)strlen(grid[row]); col++) {
      if(grid[row][col]==word[0]) {
	char temp[strlen(word)];
	for (int a = 0; a<(int)strlen(word);a++)
	  temp[a] = grid[row+a][col];
	temp[strlen(word)] = 0;
	if(strcmp(temp,word)==0) {
	  fprintf(write_to,"%s %d %d D\n",word,row,col);
	  isFound++;
	}
      }
    }
  }
  return isFound;

}


/* 
 * Returns the number of times the given word was found in the grid facing up 
 */
int find_up   (char grid[][MAX_SIZE+2], int n, char word[], FILE *write_to){
  int isFound = 0;
  for (int row = (int)strlen(word)-1; row<n; row++) {
    for (int col = 0; col<(int)strlen(grid[row]); col++) {
      if(grid[row][col]==word[0]) {
	char temp[strlen(word)];
	for (int a = 0; a<(int)strlen(word); a++)
	  temp[a] = grid[row-a][col];
	temp[strlen(word)] = 0;
	if(strcmp(temp,word)==0) {
	  fprintf(write_to,"%s %d %d U\n",word,row,col);
	  isFound++;
	}
      }
    }
  }
  return isFound;

}


/* 
 * Returns the total number of times the given word was found in the grid 
 */
int find_all  (char grid[][MAX_SIZE+2], int n, char word[], FILE *write_to) {
  int sum=0;
  for (int a = 0; a<(int)strlen(word); a++)
    word[a]= tolower(word[a]);
  sum=sum+find_right(grid,n,word,write_to);
  sum=sum+find_left(grid,n,word,write_to);
  sum=sum+find_down(grid,n,word,write_to);
  sum=sum+find_up(grid,n,word,write_to);
  if(sum==0) {
     fprintf(write_to,"%s - Not Found\n",word);
  }
  return sum;

} 


/*
 * Reads lhs and rhs character by character until either reaches eof.
 * Returns true if the files can be read and the two files match
 * character by character. Returns false if two files either can't be
 * opened or don't match. The definition of this function is provided 
 * for you.
 */
int file_eq(char lhs_name[], char rhs_name[]) {
  FILE* lhs = fopen(lhs_name, "r");
  FILE* rhs = fopen(rhs_name, "r");

  // don't compare if we can't open the files
  if (lhs == NULL || rhs == NULL) return 0;

  int match = 1;
  // read until both of the files are done or there is a mismatch
  while (!feof(lhs) || !feof(rhs)) {
	if (feof(lhs) ||                      // lhs done first
		feof(rhs) ||                  // rhs done first
		(fgetc(lhs) != fgetc(rhs))) { // chars don't match
	  match = 0;
	  break;
	}
  }
  fclose(lhs);
  fclose(rhs);
  return match;
}

