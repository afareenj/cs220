// word_search.c
// <STUDENT: ADD YOUR INFO HERE: name, JHED, etc.>
// Afareen Jaleel
// ajaleel3


#include <stdio.h>
#include <string.h>
#include "search_functions.h"


/*
 * This is the HW3 main function that performs a word search.
 */
int main(int argc, char* argv[]) {

  // Fill in your main code here!
  if(argc<2) {
    printf("Please enter a command line argument.");
    return -1;
  }
  char g[MAX_SIZE+2][MAX_SIZE+2]; //define grid array g
  int rows = populate_grid(g,argv[1]);//call function populate grid with parameters g and argv[1]
  char w[MAX_SIZE+2];
  //char *w;
  FILE *file;
  //file = fopen("output.txt","w");
  file = stdout;
  while (scanf(" %s",w)==1) {
    find_all(g,rows,w,file);
  }
  fclose(file);
  return 0;
}

