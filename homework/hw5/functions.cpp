//functions.cpp
//Afareen Jaleel
//ajaleel3
#include <cctype>
#include <iostream>
#include "functions.h"
using std::string;
using std::vector;
using std::map;
using std::cout;
using std::endl;

//function that edits words read from file
//converts all chars to lowercase and removes punctuation
void editLines(string& line) {
  string w = "";
  for(int i=0; i<(int)line.length(); i++) {
    if(isupper(line[i]))
      w+=tolower(line[i]);
    else if(!ispunct(line[i]))
      w+=line[i];
  }
  line = w;
}
//function that checks if digraph is in word
//returns true if digraph is found in word, false if it is not
bool findDigraph(string line, string syllable) {
  bool b = false;
  if(line.find(syllable)!=string::npos) {
    b = true;
  }
  return b;
}
//function that outputs digraphs in ASCII order and corresponding words with digraph
void aDigraph(map<string,vector<string>> digraph) {
  for(map<string,vector<string>>::iterator i = digraph.begin(); i!=digraph.end();++i) {
      cout<<i->first<<": [";
      for(vector<string>::iterator it = (i->second).begin(); it!=(i->second).end(); ++it) {
	if(it==(i->second.begin()))
	   cout<<*it;
	else if(it==(i->second).end()-1)
	  cout<<*it;
	else
	  cout<<*it<<", ";
      }
      cout<<"]"<<endl;
  }
}
//function that outputs digraphs in reverse ASCII order and corresponding words with digraph
void rDigraph(map<string,vector<string>> digraph) {
  for(map<string,vector<string>>::reverse_iterator i = digraph.rbegin(); i!=digraph.rend();++i) {
      cout<<i->first<<": [";
      for(vector<string>::iterator it = (i->second).begin(); it!=(i->second).end(); ++it) {
        if(it==(i->second.begin()))
           cout<<*it;
        else if(it==(i->second).end()-1)
          cout<<*it;
        else
          cout<<*it<<", ";
      }
      cout<<"]"<<endl;
  }
}
//function that inputs values into map that organizes digraphs by order according to how many times they appear in words in the file  
void createNumOrder(map<string,vector<string>> digraph, map<int,vector<string>>& order) {
  int count;
  for(map<string,vector<string>>::iterator i = digraph.begin(); i!=digraph.end();++i) {
    count = (int)(i->second).size()-1;
    order[count].push_back(i->first); 
      
  }
}
//function that prints digraphs according to the number of times they appear in words in the file from highest to lowest
void cDigraph(map<string,vector<string>> digraph,map<int, vector<string>> order) {
  for(map<int,vector<string>>::reverse_iterator i = order.rbegin(); i!=order.rend();++i) {
      for(vector<string>::iterator it = (i->second).begin(); it!=(i->second).end(); ++it) {
	vector<string> temp = digraph[*it];
	cout<<*it<<": [";
	for(vector<string>::iterator ite = temp.begin(); ite!=temp.end(); ++ite) {
	  if(ite==(temp.begin()))                                                                                                            
           cout<<*ite;
	  else if(ite==temp.end()-1)                                                                                                         
	    cout<<*ite;
	  else
	    cout<<*ite<<", ";
	}
	cout<<"]"<<endl;
      }
  }
  
}
//function that filters the query entered by the user
//returns an int if the query is an int
//returns -1 if the query is a digraph (string) and converts chars to lowercase
int filterQuery(string& query) {
  int num = -1;
  if (isdigit((query)[0])) {
    num = stoi(query);
  }
  else {
    for(int a = 0; a < (int)(query).length(); a++) {
      if(isupper((query)[a]))
	(query)[a] = tolower((query)[a]);
    }
  }
  return num;
}
//function that prints digraphs that appear query number of times (when int entered as query) 
void numQuery(int num, map<string,vector<string>> digraph,map<int, vector<string>> order) {
  if(order.count(num)>0) {
    vector<string> temp = order[num];
    for(vector<string>::iterator i = temp.begin(); i!=temp.end(); ++i) {
      cout<<*i<<endl;
      vector<string> temp2 = digraph[*i];
      for(vector<string>::iterator it = temp2.begin(); it!=temp2.end(); ++it) {
	if(it!=temp2.begin())
	  cout<<*it<<endl;
      }
    }
  }
  else
    cout<<"None"<<endl;
}
//function that prints number of times the query appeared and the words with query as digraph (when string entered as query)
void diQuery(string query, map<string,vector<string>> digraph) {
  if(digraph.count(query)>0) {
    vector<string> temp = digraph[query];
    cout<<(int)temp.size()-1<<endl;
    for(vector<string>::iterator it = temp.begin(); it!=temp.end(); ++it) {
        if(it!=temp.begin())
          cout<<*it<<endl;
    }
  }
  else
    cout<<"No such digraph"<<endl;

}
