//digraphAnalyzer.cpp                                                                  
//Afareen Jaleel
//ajaleel3
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <map>
#include <vector>
#include "functions.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::map;
using std::vector;

int main(int argc, char* argv[]) {
  if(argc<3)
    cout<<"You did not enter the correct number of arguments."<<endl;
  std::ifstream file;
  file.open(argv[1], std::ifstream::in); //open file
  string line; //string representing the words being extracted from the file 
  int numDigraphs; //int representing the number of digraphs (first int in file)
  vector<string> syllables; //vector of digraphs
  map<string,vector<string>> digraphs; //map of digraphs and the words in file that have the digraphs
  if(file.is_open()) { //if the file is open
    file>>numDigraphs;//read in int representing the number of digraphs to be analyzed in the file 
    for (int a = 0; a < numDigraphs; a++) {//loop to read in digraphs
      file>>line;//read in each word from the file 
      editLines(line);//call function to edit the digraph to be all lowercase 
      syllables.push_back(line);//add the digraph to the vector of digraphs
      digraphs[syllables[a]].push_back(""); //add empty string to initialize value for vector of strings in map digraphs
    }
    while(file>>line){//while words are read from file
      editLines(line);//call function to edit the digraph to be all lowercase
      for (int b = 0; b < numDigraphs; b++) {//loop through all the digraphs to be found
	if(findDigraph(line,syllables[b])) {//call function that returns true if digraph is found in word
	  digraphs[syllables[b]].push_back(line);//add word to vector of words in map digraphs that corresponds to the digraph it has
	}
      }
    }
    map<int,vector<string>> numOrder;//declare new map numOrder to represent the number of times a particular digraph occurs
    createNumOrder(digraphs,numOrder);//call function that uses data in digraphs to construct map numOrder 
    string userChoice = (string)argv[2];//string for user choice for displaying digraphs in command line argument
    if(userChoice.compare("a")==0)//if user entered a as command line argument, calls function that prints digraphs in ASCII order
      aDigraph(digraphs);
    else if(userChoice.compare("r")==0)//if user entered r as command line argument, calls function that prints digraphs in reverse order
      rDigraph(digraphs);
    else if(userChoice.compare("c")==0)//if user entered c as command line argument, calls function that prints digraphs in count order
      cDigraph(digraphs,numOrder);
    else
      cout<<"You did not enter a valid argument to display the digraphs!"<<endl;//if user did not enter a, r, or c, prints error statement
    string query;
    do{
      cout<<"q?>";
      cin>>query;
      int num = filterQuery(query);//calls function that returns num if string query holds an int, or returns -1 and turns characters to lowercase if it is a string   
      if(num!=-1)//if query is an int 
	numQuery(num,digraphs,numOrder);//call function that outputs digraphs that appears that num of times in file and corresponding words with that digraph 
      else if(query.compare("quit")!=0)//if query is a string (digraph) and not quit
	diQuery(query,digraphs);//call function that outputs number of words in file with that digraph and outputs the words with that digraph
    }while(query.compare("quit")!=0);//continue loop until user enters "quit"
    file.close();
  }
  else {
    cout<<"Unable to open file"<<endl;//return error statement if input file could not be opened
    return 1;
  }
  return 0;
}
