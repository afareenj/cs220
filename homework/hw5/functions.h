//functions.h
//Afareen Jalel
//ajaleel3

#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <string>
#include <vector>
#include <map>

void editLines(std::string& line);
bool findDigraph(std::string line,std::string syllable);
void aDigraph(std::map<std::string,std::vector<std::string>> digraph);
void rDigraph(std::map<std::string,std::vector<std::string>> digraph);
void createNumOrder(std::map<std::string,std::vector<std::string>> digraph, std::map<int,std::vector<std::string>>& order);
void cDigraph(std::map<std::string,std::vector<std::string>> digraph,std::map<int, std::vector<std::string>> order);
int filterQuery(std::string& query);
void numQuery(int num, std::map<std::string,std::vector<std::string>> digraph, std::map<int,std::vector<std::string>> order);
void diQuery(std::string query, std::map<std::string,std::vector<std::string>> digraph);  
#endif
