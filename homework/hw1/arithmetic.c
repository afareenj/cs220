///Name: Afareen Jaleel
//JHED: ajaleel3
#include <stdio.h>
int main() {
  float num1 = 1;
  float answer = 1;
  char op;
  printf("Please enter an arithmetic expression using * and / only: \n");
  scanf(" %f",&answer);
  while(scanf(" %c%f",&op,&num1)==2) {
    if(op=='*') {
      answer = answer*num1;
    }         
    else if(op=='/') {
      if(num1==0) {
	printf("division by zero\n");
	return 2;
      }
      else {
	answer = answer/num1;
      }
    }
    else {
      printf("malformed formula\n");
      return 1;
    }	
  }
  if(scanf(" %c",&op)==1) {
      printf("malformed formula\n");
      return 1;
   }
  printf("%f\n",answer);  
  return 0;
}
