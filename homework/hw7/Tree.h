//Tree.h
#ifndef TREE_H
#define TREE_H

#include <string>
#include <ostream>

// generalized tree of type T 

template<typename T>
class Tree {
  friend class TreeTest;
  T data; //the value stored in the tree node
  Tree<T> * kids;//children - pointer to first child of list
  Tree<T> * sibs;//siblings - pointer to rest of children list
  Tree<T> * prev;//pointer to parent if this is first child, or left sibling
 public:
  Tree(T ch);//constructor
  ~Tree();//destructor
  Tree& operator^(Tree& rt);//^ operator to addChild
  bool addChild(T ch);//adds child to tree, returns false if not unique
  bool addChild(Tree *root);//add tree root as child, unless invalid root or not unique
  std::string toString();//all values in tree, separated by newlines
  template<typename U>
    friend std::ostream& operator<<(std::ostream& os, Tree& rt);//operator that outputs toString to stream
 private:
  bool addSibling(T ch); //called by addChild(T ch), returns false if not unique
  bool addSibling(Tree *root);//called by addChild(Tree *root) returns false if invalid root or not unique 
};
#include "Tree.inc"
#endif
