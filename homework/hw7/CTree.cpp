#include "CTree.h"
using std::ostream;
using std::string;

//overloaded << operator that displays value of toString to output stream
ostream& operator<<(ostream& os, CTree& rt) {
  os<<rt.toString();
  return os;
}
//constructor of new CTree, passed character to store in root node
CTree::CTree(char ch) {
  data = ch;
  kids = nullptr;
  sibs = nullptr;
  prev = nullptr;
}
//destructor of CTree
CTree::~CTree() {
  if(!(kids==nullptr))
    delete kids;
  if(!(sibs==nullptr))
    delete sibs;
}
//returns a string of all the elements in nodes of tree, separated by \n
string CTree::toString() {
  string temp;
  temp = "";
  temp.push_back(data);
  temp+="\n";
  if(kids!=nullptr) {
    temp+=kids->toString();
  }
  if(sibs!=nullptr) {
    temp+=sibs->toString();
  }
  return temp;
}
//overloaded operator that calls addChild and returns CTree
CTree& CTree::operator^(CTree& rt) {
  CTree *temp = &rt;
  if(this->addChild(temp))
    return *this;
  else
    return *this;
}
//adds character as child in tree according to ASCII order
//returns false if failure (because ch is already child)
bool CTree::addChild(char ch) {
  if(kids==nullptr) {
    kids = new CTree(ch);
    kids->prev = this;
    return true;
  }
  else if(ch<kids->data) {
    CTree *temp = kids;
    kids = new CTree(ch);
    kids->prev = this;
    kids->sibs = temp;
    return true;
  }
  else {
    CTree *temp = kids;
    if(temp->addSibling(ch)) {
      kids = temp;
      kids->prev = this;
      return true;
    }
  }
  return false;
}
//function called by addChild(char ch) to insert ch into tree according to ASCII order
//returns false if failure (because ch is already a sibling)
bool CTree::addSibling(char ch) {
  if(data==ch || prev==nullptr) {
    return false;
  }
  else if(sibs==nullptr){
    sibs = new CTree(ch);
    sibs->prev = this;
    return true;
  }
  else if(ch<sibs->data) {
    CTree *t = sibs;
    sibs = new CTree(ch);
    sibs->prev = this;
    sibs->sibs = t;
    return true;
  }
  else {
    CTree *temp = sibs;
    return temp->addSibling(ch);
  }
}
//adds root to be stored as child of node
//returns false if root is invalid or data is already child of node
bool CTree::addChild(CTree *root) {
  if(root->sibs==nullptr && root->prev==nullptr) {
    if(kids==nullptr) {
      kids = root;
      kids->prev = this;
      return true;
    }
    else if(root->data<kids->data) {
      CTree *temp = kids;
      kids = root;
      kids->prev = this;
      kids->sibs = temp;
      return true;
    }
    else {
      CTree *temp = kids;
      if(temp->addSibling(root)) {
	kids = temp;
	kids->prev = this;
	return true;
      }
      
    }
  }
  return false;
}
//function called by addChild(CTree *root) to insert root into tree according to ASCII order
//returns false if failure (because root is invalid or data is already sibling of node)
bool CTree::addSibling(CTree *root) {
  if(root->sibs==nullptr && root->prev==nullptr) {
    if(data==root->data || prev==nullptr)
      return false;
    else if(sibs==nullptr){
      sibs = root;
      sibs->prev = this;
      return true;
    }
    else if(root->data < sibs->data) {
      CTree *t = sibs;
      sibs = root;
      sibs->prev = this;
      sibs->sibs = t;
      return true;
    }
    else {
      CTree *temp = sibs;
      return temp->addSibling(root);
    }
  }
  return false;
}
