//Tree.inc
#include <sstream>

//constructor of new Tree, passed T ch to store in root node
template<typename T>
Tree<T>::Tree(T ch) {
  data = ch;
  kids = nullptr;
  sibs = nullptr;
  prev = nullptr;
}
//destructor of Tree
template<typename T>
Tree<T>::~Tree<T>() {
  if(!(kids==nullptr))
    delete kids;
  if(!(sibs==nullptr))
    delete sibs;
}
//overloaded operator that calls addChild and returns Tree
template<typename T>
Tree<T>& Tree<T>::operator^(Tree<T>& rt) {
  Tree<T> *temp = &rt;
  if(this->addChild(temp))
    return *this;
  else
    return *this;
}
//adds T ch as child in tree according to order
//returns false if failure (because ch is already child)
template<typename T>
bool Tree<T>::addChild(T ch) {
  if(kids==nullptr) {
    kids = new Tree<T>(ch);
    kids->prev = this;
    return true;
  }
  else if(ch<kids->data) {
    Tree<T> *temp = kids;
    kids = new Tree<T>(ch);
    kids->prev = this;
    kids->sibs = temp;
    return true;
  }
  else {
    Tree<T> *temp = kids;
    if(temp->addSibling(ch)) {
      kids = temp;
      kids->prev = this;
      return true;
    }
  }
  return false;
}
//adds root to be stored as child of node
//returns false if root is invalid or data is already child of node
template<typename T>
bool Tree<T>::addChild(Tree<T> *root) {
  if(root->sibs==nullptr && root->prev==nullptr) {
    if(kids==nullptr) {
      kids = root;
      kids->prev = this;
      return true;
    }
    else if(root->data<kids->data) {
      Tree<T> *temp = kids;
      kids = root;
      kids->prev = this;
      kids->sibs = temp;
      return true;
    }
    else {
      Tree<T> *temp = kids;
      if(temp->addSibling(root)) {
        kids = temp;
	kids->prev = this;
	return true;
      }
    }
  }
  return false;
}
//function called by addChild (T ch) to insert ch into tree according to order
//returns false if failure (because ch is already a sibling)
template<typename T>
bool Tree<T>::addSibling(T ch) {
  if(data==ch || prev==nullptr) {
    return false;
  }
  else if(sibs==nullptr) {
    sibs = new Tree<T>(ch);
    sibs->prev = this;
    return true;
  }
  else if(ch<data) {
    Tree<T> *t = sibs;
    sibs = new Tree<T>(ch);
    sibs->prev = this;
    sibs->sibs = t;
    return true;
  }
  else {
    Tree<T> *temp = sibs;
    return temp->addSibling(ch);
  }
}
//function called by addChild(Tree *root) to insert root into tree according to order
//returns false if failure (because root is invalid or data is already sibling of node) 
template<typename T>
bool Tree<T>::addSibling(Tree<T> *root) {
  if(root->sibs==nullptr && root->prev==nullptr) {
    if(data==root->data || prev==nullptr)
      return false;
    else if(sibs==nullptr) {
      sibs = root;
      sibs->prev = this;
      root->prev = this;
      return true;
    }
    else if(root->data < sibs->data) {
      Tree<T> *t = sibs;
      sibs = root;
      sibs->prev = this;
      sibs->sibs = t;
      return true;
    }
    else {
      Tree<T> *temp = sibs;
      return temp->addSibling(root);
    }
  }
  return false;
}
//returns a string of all the elements in nodes of tree, separated by \n
//reads in the values using << operator in string stream, which is then converted to string
template<typename T>
std::string Tree<T>::toString() {
  std::string temp;
  temp = "";
  std::ostringstream oss;
  oss<<data;
  temp+=oss.str();
  temp+="\n";
  if(kids!=nullptr) {
    temp+=kids->toString();
  }
  if(sibs!=nullptr) {
    temp+=sibs->toString();
  }
  return temp;
}
//overloaded << operator that displays value of toString in output stream
template<typename T>
std::ostream& operator<<(std::ostream& os, Tree<T>& rt) {
  os<<rt.toString();
  return os;
}