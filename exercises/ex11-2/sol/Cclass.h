#include <iostream>
#include <string>

#ifndef _CCLASS_H
#define _CCLASS_H

#include "Aclass.h"

class C : public A {
private:
  int e;

public:
  C(int val = 0): A(), e(val) { };  // automatically sets a & d to 0 w/ A()
  void sete(int val) { e = val; };
  virtual int fun() const override { return geta() * e * d; }
  virtual std::string toString() const override{ std::stringstream ss; ss << "[C: a = "; ss << geta(); ss << ", d = "; ss << d; ss << ", e = "; ss << e; ss <<", size = "; ss << sizeof(*this);  ss << "]"; return ss.str(); };
};


#endif
