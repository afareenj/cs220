#include <iostream>
#include <string>
#include <sstream>

#ifndef _ACLASS_H
#define _ACLASS_H

class A { // base class
private: 
  int a;

protected:
  double d;

public: 
  A(int val = 0, double num = 0): a(val), d(num) { };
  void seta(int val) { a = val; };
  void setd(double dval) { d = dval; } ;
  int geta() const { return a; }
  virtual int fun() const = 0;
  virtual std::string toString() const { std::stringstream ss; ss << "[A: a = "; ss << a; ss << ", d = "; ss << d; ss << ", size = "; ss << sizeof(*this); ss <<"]"; return ss.str(); };  
};

#endif
