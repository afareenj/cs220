//Aclass.cpp

#include <sstring>
#include <string>

using std::string;
using std::stringstream;


A::string toString() {
    stringstream s;
    s << "Aclass: a = " << a << ", d = " << d << ", size = " << size;
    return s.str();
};
