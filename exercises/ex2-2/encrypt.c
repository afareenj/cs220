//encrypt.c

#include <stdio.h>
#include <string.h>
#include <math.h>

int main() {

  // read n
  int n = -1;// n used in encoding
  printf ("Enter n: ");
  if (scanf("%d", &n) != 1 || n <= 0) {
    printf("Invalid n!\n");
    return 1;
  }
  

  // collect message as a string of at most 32 characters
  char char_msg[33]; // this will hold the binary message as a string

  printf ("Enter the message to be encrypted in binary format (max 32 bit): ");
  if (scanf("%s", char_msg) != 1) { // read the binary message as a string
    printf("Could not read the message!\n");
    return 1;
  }
  
  int len = (int)strlen(char_msg); // length of the binary message
  if (len > 32) { // we would like to rule out a message longer than 32 bits
    printf("Message length should be less than 33! \n");
    return 1;
  }

  
  // TODO: convert the string binary message to an integer,
  // e.g. the message 1001011 will be stored in bin_msg as 75
  int bin_msg = 0;// the binary message as an int
  for (int i = 0; i<(int)strlen(char_msg); i++) {
    if(char_msg[i]=='1')
      bin_msg = bin_msg + pow(2,i);
  }          

  // TODO: do the encryption by shifting n - 1 times and XORing the corresponding columns
  int result;
  int temp[n];
  for (int i = 1; i<n; i++) {
    temp[i-1] = bin_msg<<i;
    //result = result^temp;
  }
  
  result = temp[0]^temp[1]^temp[2]^temp[3];
  printf("shifted message of %d is %d",bin_msg,result);

  

  // TODO: convert back the encrypted message to a string
  char char_encrypted[33];
  int counter = 0;
  while(result>1) {
    char_encrypted[counter] = '0'+(result&1);
    result = result>>1;
    counter++;
  }
  char_encrypted[counter] = 0;
  //for(int i = 0; i<(int)strlen(char_encrypted);i++)
  //  printf("%c",char_encrypted[i]);



  
  // print out the original and encrypted messages as strings
  printf("Plain binary message is %s and encrypted message is %s \n", char_msg, char_encrypted);

  return 0;
}
