#include <stdio.h>
#include <string.h>
#include "string_functions.h"

/*
    Returns in the third argument the concatenation of the first
    argument and the second argument, provided that there is
    sufficient space in third argument, as specified by the fourth.
    e.g.
        concat("alpha", "beta", result, 10) puts "alphabeta" into result and returns 0
        concat("alpha", "gamma", result, 10) puts nothing into result and returns 1
*/
int concat(const char word1[], const char word2[], char result[], int result_capacity){

   //TODO: replace the following stub with a correct function body so that 
   //      this function behaves as indicated in the comment above
   //
   //NOTE: you may not use the strcat or strcpy library functions in your solution!
  result_capacity = strlen(word1)+strlen(word2)+1;
  if(result_capacity<21) {
  for (int a=0;a<(int)strlen(word1);a++) {
    result[a] = word1[a];
  }
  int a = (int)strlen(word1);
  for (int b=0;b<(int)strlen(word2)+1;b++) {
    result[a+b] = word2[b]; 
  }
  result[result_capacity]='\0';
  return -1;
  }
  else {
    printf("The words you entered were too long\n");
    return 1;
  }

}
  /* Note: What happens if the user enters more than 10 characters
           when prompted for word1 or word2?  After you finish the
           rest of this exercise, try it out.

           We need to learn a safer way to collect strings from the user...stay tuned!
  */

