/**
 * Hint: resolve the TODOs in grade_list.h first.
 * 
 * TODO: Write a program that declares a GradeList
 *       variable and adds to it all the even
 *       numbers 0-100:
 *       
 *       {0, 2, 4, ..., 98, 100}
 * 
 *       then prints out the minimum, maximum,
 *       median, mean and 75th percentile, all
 *       nicely labelled.
 */
#include "grade_list.h"
#include <iostream>

using std::cout;
using std::endl;

int main(void) {

  GradeList g1;

  for(int i = 0; i <= 50; i++) {
    g1.add(2*i);
  }  

  double min = 100.0;
  double max = 0.0;

  for (int i = 0; i < (int)g1.numEl(); i++) {
    if (g1.ind(i) < min)
      min = g1.ind(i);
    if (g1.ind(i) > max)
      max = g1.ind(i);
  }
  cout << "Minimum grade is: " << min << endl;
  cout << "Maximum grade is: " << max << endl;
  cout << "Median is: " << g1.median() << endl;
  cout << "Mean is: " << g1.mean() << endl;
  cout << "75th percentile is: " << g1.percentile(75) << endl;

  
  return 0;
}
