#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "read_line.h"

/*
 * This function reads the next line from the file handle fp.
 * (That is, it reads either up to the next newline character, or to the 
 * end of the file, whichever comes first.) If fp is at the end of the 
 * file when the function is called, the function returns NULL.
 * Otherwise, it returns the null-terminated string that was read 
 * (including the newline character, if line didn't end at end of file).
 */
char* read_line(FILE* fp) {

  // The function is implemented by making successive calls to the fgets function
  // and dynamically growing a character array that contains the line.
  
  char buffer[10];   // The buffer into which fgets reads. (Do _not_ change its size.)
  char* line = NULL; // The character array / string that stores the line


  // Repeatedly call fgets until you have read the entire line (or until you reach 
  // the end-of-file). If fp is afgets returns NULL.
  while (fgets( buffer, 10, fp)) {

    if (line == NULL) {  //it's our first time through!

      line = (char *) malloc(strlen(buffer) + 1);
      strcpy(line, buffer);

    } else {

      char* temp_line = (char *) malloc(strlen(line) + strlen(buffer) + 1);
      strcpy(temp_line, line);
      strcat(temp_line, buffer);
      free(line);
      line = temp_line;
    }

    if (line[strlen(line) - 1] == '\n') {
      return line;
    }

  }

  return line;
}
