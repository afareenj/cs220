#include <stdio.h>
#include <string.h>
#include "soccer.h"



int main() {

  const int TEAMSIZE = 11;
  Player team[TEAMSIZE];
  Stat stat;

  create_team(team, TEAMSIZE);

  printf("Before Update:\n");
  print_team(team, TEAMSIZE);
  printf("Enter valid number of goals, number of assists, and pass accuracy:\n");

  //TODO 2: READ AND STORE STAT INFO TO stat 
  scanf("%d %d %f",&stat.num_of_goals,&stat.num_of_assists,&stat.pass_accuracy);
    
  
  //TODO 3: FIND THE INDEX OF PLAYER IN THE ARRAY team WITH THE LATEST SIGNED DATE. 
  //IF THERE ARE MORE THAN ONE PLAYERS WITH THE SAME SIGNED DATE, SELECT THE ONE 
  //THAT APPEARS FIRST IN THE ARRAY.
    int y = team[0].Date.year;
    int m = team[0].Date.month;
    int d = team[0].Date.day;
    int playerNum = 0;
    //printf("Player[0]: %d\n",y);
    for (int i=1;i<TEAMSIZE;i++) {
      printf("Player [%d]: %d\n",i,team[i].Date.year);
      if(team[i].Date.year>y) {
	y = team[i].Date.year;
	m = team[i].Date.month;
	d = team[i].Date.day;
	playerNum = i;
      } else if (team[i].Date.year == y && team[i].Date.month>m){
	m = team[i].Date.month;
	d = team[i].Date.day;
	playerNum = i;
      } else if (team[i].Date.year == y && team[i].Date.month == m && team[i].Date.day>d) {
	d = team[i].Date.day;
	playerNum = i;
      }
    }
  
  //TODO 4: CALL TO update_player_stats USING stat AND THE SELECTED PLAYER FROM THE ARRAY team
    update_player_stats(stat, &team[playerNum]);
    
    
  printf("After Update:\n");
  print_team(team, TEAMSIZE);

  return 0;
}
