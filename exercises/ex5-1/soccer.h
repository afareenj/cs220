#ifndef SOCCER_H
#define SOCCER_H

#include <stdbool.h>

//TODO 1: IMPLEMENT THE STRUCT TYPES Stats, Date, and Player HERE
typedef struct {
  int num_of_goals;
  int num_of_assists;
  float pass_accuracy;
} Stat;
typedef struct {
  int day;
  int month;
  int year;
} Date;
typedef struct {
  int age;
  int jersey_num;
  bool goalkeeper;
  //Stat* s;
  //Date* d;
  struct {
    int num_of_goals;
    int num_of_assists;
    float pass_accuracy;
    } Stat;
  struct {
    int day;
    int month;
    int year;
    } Date;
} Player;


void update_player_stats(Stat s, Player *p);
void create_player (Player *);
void create_team(Player [], int);
void print_team (Player [], int);

#endif
