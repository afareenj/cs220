#include "complex.h"
#include <iostream>

//adds other to this number and returns a new complex number object representing its sum
Complex Complex::add(Complex& other) const {
  other.setReal(real + other.getReal());
  other.setImag(imag + other.getImag());
  return other;
}
//outputs to standard output the complex number in the form a + bi
void Complex::output() const {
  std::cout<<real<<" + "<<imag<<"i"<<std::endl;

}
