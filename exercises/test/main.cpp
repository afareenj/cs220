#include "complex.h"
#include <iostream>

using namespace std;

int main() {
  Complex mine = Complex(1.5,6.5);
  mine.output();
  Complex yours = Complex(1.1,4.1);
  yours.output();
  mine = mine.add(yours);
  mine.output();
  
  return 0;
}
