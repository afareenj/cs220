
#ifndef COMPLEX_H
#define COMPLEX_H

class Complex {
 private:
  double real; //real part
  double imag; //imaginary part
 public:
  Complex(double r, double i):real(r),imag(i) {}
  double getReal() const {return real;}
  double getImag() const {return imag;}
  void setReal(double r) {real = r;}
  void setImag(double i) {imag = i;}
  Complex add(Complex& other) const;
  void output() const;
};
#endif
