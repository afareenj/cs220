//boundedstack.h
#ifndef _BOUNDEDSTACK_H
#define _BOUNDEDSTACK_H

class BoundedStack : public Stack {

 public:
  BoundedStack(int capacity) {
    data = new double[capacity];
    cap = capacity;
    size = capacity;
  }

}

#endif
