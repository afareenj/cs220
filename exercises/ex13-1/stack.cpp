#include <stdexcept>
#include <exception>
#include <iostream>
#include "stack.h"

using std::istream;  using std::ostream;


/****** UNCOMMENT AND FILL IN DURING PART 2*/

double Stack::pop() {
  try {
    double temp = data[size];
    size = size-1;
    return temp;
  }
  catch(const std::underflow_error& ex) {
    cout << "Got an underflow error!" << std::endl << ex.what() << std::endl;
  }
}

double Stack::top() const {
  try {
    return data[size];
  }
  catch(const std::underflow_error& ex) {
    cout << "Got an underflow error!" << std::endl << ex.what() << std::endl;
  }
  
}

void Stack::push(double d) {
  try {
    data[size+1] = d;
    size = size+1;
  }
  catch(const std::overflow_error& ex) {
    cout << "Got an overflow error!" << std::endl << ex.what() << std::endl;
  }
}





/****** UNCOMMENT AND FILL IN DURING PART 3*/


std::ostream& operator<<(std::ostream& os, const Stack& s) {
  
}

std::istream& operator>>(std::istream& is, Stack& s) {

}



